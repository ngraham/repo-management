#!/usr/bin/python
# Repository mirror updater.
# Invokes git in a custom manner and transfers across only branch and tag changes. For use with services such as Github.com and GitLab.com
import re
import sys
import argparse
import subprocess

# Read in our parameters
parser = argparse.ArgumentParser(description='Utility to mirror a repository up to Github and GitLab.com, skipping refs they do not accept')
parser.add_argument('--skip-refs', help='Regular expression determining which refs should be skipped in addition to those we normally skip (advanced users only)')
parser.add_argument('--git-arguments', help='Additional parameters to be provided to Git when pushing the changes to the remote repository (advanced users only)', default='')
parser.add_argument('target', help='URL to the remote repository we are targeting')
args = parser.parse_args()

# Find out what has supposedly changed....
# Note: it must be run under a shell as git is silly
command = "git push --mirror -n --porcelain '{0}'".format( args.target )
process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
refs_changed = process.stdout.readlines()

changes = []
for ref in refs_changed:
    # Make this line of information something we can work with
    ref = ref.decode('utf-8')
    # Look to see if it is done to a branch or tag....
    # Everything else is not relevant - and should be ignored....
    match = re.match('^[-|*| |+]\t(.*refs/(heads|tags)/\S+)\t(.+)\n', ref)
    if not match:
        continue

    # Extract the reference we have found
    remote_reference = match.group(1)

    # Do we need to check for an additional reference to skip?
    if args.skip_refs and re.match( ".*" + args.skip_refs, remote_reference):
        continue

    # Finally we know we are good to accept this change
    changes.append( remote_reference )

# Check if we actually need to do anything
if changes == []:
    sys.exit()

# Commence the mirror updating!
updating_refs = ' '.join(changes)
command = "git push {0} -f '{1}' {2}".format( args.git_arguments, args.target, updating_refs )
process = subprocess.Popen(command, shell=True)
process.wait()
